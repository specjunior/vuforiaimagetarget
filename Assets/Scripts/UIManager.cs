using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class UIManager : MonoBehaviour
{
	[SerializeField] private CannonWindow cannonWindow;
	[SerializeField] private TargetWindow targetWindow;
	[SerializeField] private ScoreWindow scoreWindow;

	public void OpenCannonWindow(Cannon cannon)
	{
		cannonWindow.SetData(cannon.Rotation, cannon.Angle, cannon.MaxAngle);
		cannonWindow.Open();
		targetWindow.Close();
	}

	public void OpenTargetWindow()
	{
		cannonWindow.Close();
		targetWindow.Open();
	}

	public void OpenScoreWindow(List<int> scores)
	{
		CloseAllWindows();
		scoreWindow.SetData(scores);
		scoreWindow.Open();
	}

	public void CloseAllWindows()
	{
		cannonWindow.Close();
		targetWindow.Close();
		scoreWindow.Close();
	}
}