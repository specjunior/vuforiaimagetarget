using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TransformHolder : MonoBehaviour
{
	public Vector3 position = Vector3.one;
	public Vector3 rotation = Vector3.one;

	private void Update()
	{
		transform.position = new Vector3(
			transform.position.x * position.x,
			transform.position.y * position.y,
			transform.position.z * position.z
			);

		transform.rotation = Quaternion.Euler(
			transform.rotation.eulerAngles.x * rotation.x,
			transform.rotation.eulerAngles.y * rotation.y,
			transform.rotation.eulerAngles.z * rotation.z
			);
	}
}