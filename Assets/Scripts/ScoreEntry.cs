using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;

public class ScoreEntry : MonoBehaviour
{
	[SerializeField] private TextMeshProUGUI numberText;
	[SerializeField] private TextMeshProUGUI pointsText;

	public void SetData(int numer, int points)
	{
		numberText.text = numer.ToString();
		pointsText.text = points.ToString();
	}
}