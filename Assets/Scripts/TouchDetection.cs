using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.InputSystem;
using UnityEngine.UI;

public class TouchDetection : MonoBehaviour
{
	private void Update()
	{
		foreach(Touch touch in Input.touches)
		{
			if(!IsPointerOverUIObject(touch.position))
			{
				if(touch.phase == UnityEngine.TouchPhase.Began)
				{
					Ray ray = Camera.main.ScreenPointToRay(touch.position);
					if(Physics.Raycast(ray, out RaycastHit hit, 1000))
						hit.transform.GetComponent<TouchableObject>().OnTouched();
				}
			}
		}
	}

	private bool IsPointerOverUIObject(Vector3 touchPosition)
	{
		List<RaycastResult> results = new List<RaycastResult>();
		PointerEventData eventData = new PointerEventData(EventSystem.current) { position = touchPosition };
		EventSystem.current.RaycastAll(eventData, results);
		return results.Count > 0;
	}
}