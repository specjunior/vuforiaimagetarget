using System.Collections.Generic;
using System.Linq;
using UnityEngine;

public class Target : TouchableObject
{
	[SerializeField] private int trialsNmaxTrialsNumber = 3;
	[SerializeField] private GameObject flagObject;
	[SerializeField] private Area[] areas;
	[SerializeField] private bool drawAreasGizmo = true;
	private int points;
	private List<Vector3> shotPositions = new List<Vector3>();
	private List<GameObject> instantiatedFlags = new List<GameObject>();

	private void Start()
	{
		areas = areas.OrderBy(go => go.distance).ToArray();

		if(gameManager == null)
		{
			gameManager = FindObjectOfType<GameManager>();
			Debug.LogError("[Cannon] GameManager not assigned!", gameObject);
		}
	}

	public void ResetStats()
	{
		for(int i = 0; i < instantiatedFlags.Count; i++)
			Destroy(instantiatedFlags[i]);
		instantiatedFlags.Clear();
		shotPositions.Clear();
	}

	private int RankShootHit(Vector3 point, out int points)
	{
		float distnce = Vector3.Distance(transform.position, point);

		for(int i = 0; i < areas.Length; i++)
		{
			if(distnce <= areas[i].distance)
			{
				points = areas[i].points;
				return areas[i].points;
			}
		}

		points = 0;
		return 0;
	}

	private void OnCannonballHit(Collision other, int points)
	{
		GameObject flag = Instantiate(flagObject, transform);
		instantiatedFlags.Add(flag);
		flag.transform.position = other.contacts[0].point;
		shotPositions.Add(other.contacts[0].point);
		gameManager.AddScoreEntry(points);
	}

	private void OnCollisionEnter(Collision other)
	{
		if(other.gameObject.tag.Equals("Cannonball"))
		{
			if(!shotPositions.Contains(other.contacts[0].point))
				if(RankShootHit(other.contacts[0].point, out points) > 0)
				{
					OnCannonballHit(other, points);
					Destroy(other.gameObject);
					if(shotPositions.Count >= trialsNmaxTrialsNumber)
						gameManager.GameEnd();
				}
			Destroy(other.gameObject);
		}
	}

	private void OnDrawGizmosSelected()
	{
		if(!drawAreasGizmo) return;

		foreach(Area area in areas)
		{
			Gizmos.color = area.color;
			Gizmos.DrawWireSphere(transform.position, area.distance);
		}
	}
}

[System.Serializable]
public class Area
{
	public int points;
	public float distance;
	public Color color;
}