using System.Collections.Generic;
using UnityEngine;

public class Cannon : TouchableObject
{
	public float Rotation => rotation;
	public float Angle => angle;
	public float MaxAngle => maxAngleInDegrees;

	[SerializeField] private Transform model;
	[SerializeField] private Transform cannonBarrel;
	[SerializeField] private Transform shootPoint;
	[SerializeField] private Transform directionPoint;
	[SerializeField] private Transform trajectoryPoinsParent;

	[SerializeField] private GameObject cannonballObject;
	[SerializeField] private GameObject trajectoryPointObject;

	[SerializeField] private float maxShootForce = 20f;
	[SerializeField] private float startAngleInDegrees = 15f;
	[SerializeField] private float minAngleInDegrees = 0f;
	[SerializeField] private float maxAngleInDegrees = 90f;

	private List<GameObject> trajectoryPoints = new List<GameObject>();
	private Vector3 shootDirection;
	private Quaternion startModelRotation;
	private float rotation = 0;
	private float angle = 0;
	private float shootForce;

	private void Start()
	{
		shootForce = maxShootForce / 2f;
		startModelRotation = model.rotation;
		cannonBarrel.transform.localRotation = Quaternion.Euler(-Vector3.right * startAngleInDegrees);

		if(gameManager == null)
		{
			gameManager = FindObjectOfType<GameManager>();
			Debug.LogError("[Cannon] GameManager not assigned!", gameObject);
		}

		for(int i = 0; i < 20; i++)
			trajectoryPoints.Add(Instantiate(trajectoryPointObject, trajectoryPoinsParent));

		trajectoryPoinsParent.gameObject.SetActive(false);
	}

	private void Update()
	{
		for(int i = 0; i < trajectoryPoints.Count; i++)
		{
			trajectoryPoints[i].transform.position = PointPosition(i * 0.009f);
		}
	}

	public override void OnTouched()
	{
		base.OnTouched();
		trajectoryPoinsParent.gameObject.SetActive(true);
	}

	public override void OnDeselect()
	{
		base.OnDeselect();
		trajectoryPoinsParent.gameObject.SetActive(false);
	}

	public void Shoot()
	{
		GameObject temp = Instantiate(cannonballObject);
		temp.transform.position = shootPoint.position;

		shootDirection = (shootPoint.position - directionPoint.position).normalized;
		temp.GetComponent<Rigidbody>().AddForce(shootDirection * shootForce, ForceMode.Impulse);
	}

	public void SetShootforce(float percentage)
	{
		shootForce = maxShootForce * percentage;
	}

	public void SetAngle(float percentage)
	{
		angle = Mathf.Lerp(minAngleInDegrees, maxAngleInDegrees, percentage);
		cannonBarrel.localRotation = Quaternion.Euler(-Vector3.right * angle);
	}

	public void SetRotation(float sliderValue)
	{
		rotation = sliderValue * 180;
		model.transform.rotation = startModelRotation * Quaternion.Euler(Vector3.up * rotation);
	}

	private Vector3 PointPosition(float t)
	{
		return shootPoint.position + ((shootPoint.position - directionPoint.position).normalized * shootForce * t) + 0.5f * Physics.gravity * (t * t);
	}
}