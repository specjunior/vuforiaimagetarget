using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class UIWindow : MonoBehaviour
{
	[SerializeField] protected CanvasGroup canvasGroup;

	public virtual void Open()
	{
		canvasGroup.Enable();
	}

	public virtual void Close()
	{
		canvasGroup.Disable();
	}
}