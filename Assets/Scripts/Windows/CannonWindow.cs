using TMPro;
using UnityEngine;
using UnityEngine.UI;

public class CannonWindow : UIWindow
{
	[SerializeField] private GameManager gameManager;

	[SerializeField] private TextMeshProUGUI rotationText;
	[SerializeField] private Slider rotationSlider;
	[SerializeField] private TextMeshProUGUI angleText;
	[SerializeField] private Slider angleSlider;

	private void Start()
	{
		if(gameManager == null)
		{
			gameManager = FindObjectOfType<GameManager>();
			Debug.LogError("[CannonWindow] GameManager not assigned!", gameObject);
		}
	}

	public void SetData(float rotation, float angle, float maxAngle)
	{
		rotationText.text = ((int)rotation).ToString();
		rotationSlider.value = rotation / 180;

		angleText.text = ((int)angle).ToString("D");
		angleSlider.value = angle / maxAngle;
	}

	public void Shoot_()
	{
		gameManager.Shoot();
	}

	public void SetShootforce_(float sliderValue)
	{
		gameManager.SetShootforce(sliderValue);
	}

	public void SetAngle_(float sliderValue)
	{
		angleText.text = ((int)(sliderValue * 80)).ToString();
		gameManager.SetAngle(sliderValue);
	}

	public void SetRotation_(float sliderValue)
	{
		rotationText.text = ((int)(sliderValue * 180)).ToString();
		gameManager.SetRotation(sliderValue);
	}
}