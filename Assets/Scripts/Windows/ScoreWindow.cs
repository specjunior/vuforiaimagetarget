using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ScoreWindow : UIWindow
{
	[SerializeField] private GameManager gameManager;
	[SerializeField] private Transform entryParent;
	[SerializeField] private GameObject entryObject;

	private List<GameObject> entries = new List<GameObject>();

	private void Start()
	{
		if(gameManager == null)
		{
			gameManager = FindObjectOfType<GameManager>();
			Debug.LogError("[ScoreWindow] GameManager not assigned!", gameObject);
		}
	}

	public override void Close()
	{
		base.Close();

		for(int i = 0; i < entries.Count; i++)
			Destroy(entries[i]);
		entries.Clear();
	}

	public void Reset_()
	{
		gameManager.ResetGame();
	}

	public void SetData(List<int> scores)
	{
		scores.Sort();
		scores.Reverse();

		int i = 1;
		foreach(int score in scores)
		{
			AddEntry(i, score);
			i += 1;
		}
	}

	private void AddEntry(int number, int points)
	{
		GameObject go = Instantiate(entryObject, entryParent);
		go.GetComponent<ScoreEntry>().SetData(number, points);
		entries.Add(go);
	}
}