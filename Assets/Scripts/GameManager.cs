using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GameManager : MonoBehaviour
{
	[SerializeField] private UIManager uIManager;
	[SerializeField] private Target target;

	private List<int> scores = new List<int>();
	private TouchableObject currentSelected;

	public void Start()
	{
		uIManager.CloseAllWindows();
	}

	public void GameEnd()
	{
		//show best
		uIManager.OpenScoreWindow(scores);
	}

	public void ResetGame()
	{
		uIManager.CloseAllWindows();
		target.ResetStats();
		scores.Clear();
	}

	public void AddScoreEntry(int points)
	{
		scores.Add(points);
	}

	public void TouchedObject(TouchableObject obj)
	{
		currentSelected?.OnDeselect();
		currentSelected = obj;

		if(obj is Cannon)
			uIManager.OpenCannonWindow((Cannon)obj);
		else if(obj is Target)
			uIManager.OpenTargetWindow();
	}

	public void LostObject(TouchableObject lostObj)
	{
		if(lostObj.Equals(currentSelected))
		{
			currentSelected = null;
			uIManager.CloseAllWindows();
		}
	}

	public void Shoot()
	{
		((Cannon)currentSelected).Shoot();
	}

	public void SetShootforce(float percentage)
	{
		((Cannon)currentSelected).SetShootforce(percentage);
	}

	public void SetAngle(float percentage)
	{
		((Cannon)currentSelected).SetAngle(percentage);
	}

	public void SetRotation(float sliderValue)
	{
		((Cannon)currentSelected).SetRotation(sliderValue);
	}
}