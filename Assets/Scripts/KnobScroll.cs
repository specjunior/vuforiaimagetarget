using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

public class KnobScroll : MonoBehaviour
{
	private Vector3 mousePos;

	public KnobEvent OnValueChange;

	public void OnDrag()
	{
		mousePos = Input.mousePosition;
		Vector2 direction = mousePos - transform.position;
		float angle = Mathf.Atan2(direction.y, direction.x) * Mathf.Rad2Deg;
		angle = (angle <= 0) ? (360 + angle) : angle;
		if(angle <= 255 || angle >= 315)
		{
			Quaternion r = Quaternion.AngleAxis(angle + 135f, Vector3.forward);
			transform.rotation = r;
			angle = angle > 315 ? angle - 360 : angle;
			angle = 0.75f - (angle / 360f);
			OnValueChange.Invoke(Mathf.Round((angle * 100) / 0.75f) / 100f);
		}
	}
}

[System.Serializable] public class KnobEvent : UnityEvent<float> { }