using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TouchableObject : MonoBehaviour
{
	[SerializeField] protected GameManager gameManager;

	public virtual void OnTouched()
	{
		gameManager.TouchedObject(this);
	}

	public virtual void OnTargetFound()
	{
	}

	public virtual void OnTargetLost()
	{
		OnDeselect();
		gameManager.LostObject(this);
	}

	public virtual void OnDeselect()
	{
	}
}